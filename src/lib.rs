use std::io::{self, Write};
use indicatif::ProgressBar;

pub fn find_matches(content: &str, pattern: &str, mut writer: impl Write) -> io::Result<()> {
    let mut count = 0;
    for line in content.lines() {
        if line.contains(pattern) {
            writeln!(writer, "{}", line)?;
            count += 1;
        }
    }
    let pb = ProgressBar::new(count);
    pb.set_message("Processing");

    pb.finish_with_message("done");

    Ok(())
}
