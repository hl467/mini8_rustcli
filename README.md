# Rust Pattern Search CLI
### Author: Haoyan Li 
## Overview

This command-line application, written in Rust, allows users to search for specific patterns within a text file. It reads the file, searches for lines containing the specified pattern, and outputs those lines.

## Features

- **Pattern Searching:** Search for any text pattern within a file.
- **Progress Display:** Includes a progress bar while searching large files.

## Usage

The main function of the application is structured as follows:

1. Parse command-line arguments using `clap` to get the search pattern and file path.
2. Initialize a progress bar using `indicatif`.
3. Read the specified file.
4. Call `find_matches` function to search for the pattern in the file content.
5. Print the lines that contain the pattern.
6. Display a completion message with the progress bar.

To use the application, run:

```
cargo run -- [PATTERN] [FILE PATH]
```

- `[PATTERN]`: The pattern you are searching for in the file.
- `[FILE PATH]`: The path to the file where the search will be performed.

Example:

```
cargo run -- "lhy" sample.txt
```

This will search for the pattern "lhy" in `sample.txt`.
- screenshot of the output here by manually running the command
![manual](manual.png)
- there is status bar at the end indicating the number of lines that contain the pattern

## Testing

Tests are included to ensure the application's functionality:

- **find_a_match:** Tests the `find_matches` function with a predefined string and pattern, checking the output.
- **file_doesnt_exist:** Verifies the application's behavior when the specified file does not exist.
- **find_content_in_file:** Tests the entire application by creating a temporary file with known content, running the application to find a specified pattern, and asserting the output.

Run tests with:

```
cargo test
```

- screenshot of testing
![test](test.png)

## Dependencies

- `clap`: Command-line argument parsing.
- `indicatif`: Progress bar display.
- `assert_cmd`, `predicates`, `assert_fs`: Testing utilities.

